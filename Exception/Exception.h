#ifndef SCHOOL_SYSTEM_EXCEPTION_H
#define SCHOOL_SYSTEM_EXCEPTION_H

#include <exception>

class InvalidItemNumber : public std::exception {

public:

    const char* what() const noexcept override {
        return "\nInvalid item number! Try again!";
    }

};

class InvalidStore : public std::exception {

public:

    const char* what() const noexcept override {
        return "\ngetStore was called before createStore! Got the null pointer!\n";
    }

};

class InvalidRegPassword : public std::exception {

public:

    const char* what() const noexcept override {
        return "\n\nThe password entered again does not match the original one! Try again!\n\n";
    }

};

class InvalidUser : public std::exception {

public:

    InvalidUser() noexcept : std::exception() {}

    const char* what() const noexcept override {
        return "\n\nThere is no this user in the database! Try again!\n\n";
    }

};

class InvalidUserLogin : public std::exception {

public:

    InvalidUserLogin() noexcept : std::exception() {}

    const char* what() const noexcept override {
        return "\n\nThe login is used yet! Try again!\n\n";
    }

};


#endif //SCHOOL_SYSTEM_EXCEPTION_H

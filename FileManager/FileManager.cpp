#include "FileManager.h"

string FileManager::usersFilePath{};
string FileManager::itemsFilePath{};
string FileManager::historyFilePath{};
FileManager* FileManager::instance{};

FileManager::FileManager(string usersPath, string itemsPath, string historyPath)
{
    usersFilePath = std::move(usersPath);
    itemsFilePath = std::move(itemsPath);
    historyFilePath = std::move(historyPath);
}

bool FileManager::loadItems(ItemsList & list)
{
    m_input.open(itemsFilePath, std::ios::binary);

    if (!m_input.is_open()) {
        std::cerr << "\n\nItemsData.dat didn't open! FileManager::loadItems()\n\n";
        return false;
    }

    int size{}, index{};
    char *buff{};
    itm::Item temp{};
    itm::Item *items{};

    m_input.read((char*)&itemsCount, sizeof(itemsCount)); // загружаем количество товаров
    items = new itm::Item[itemsCount]{};

    for (int i = 0; i < itemsCount; ++i) {

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.name = buff;
        delete[] buff;

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.description = buff;
        delete[] buff;

        m_input.read((char*)&temp.price, sizeof(temp.price));

        temp.id = itm::Item::current_item_id++;

        items[index++] = temp;
    }

    m_input.close();

    for (int i = 0; i < itemsCount; ++i) {
        list.push_back(&items[i]);
    }

    return true;
}

bool FileManager::loadUsers(UsersList & list)
{
    m_input.open(usersFilePath, std::ios::binary);

    if (!m_input.is_open()) {
        std::cerr << "\n\nUsersData.dat didn't open! FileManager::loadUsers()\n\n";
        return false;
    }

    int size{}, index{};
    char *buff{};
    User temp{};
    User *users{};

    m_input.read((char*)&usersCount, sizeof(usersCount)); // загружаем количество товаров
    users = new User[usersCount]{};

    for (int i = 0; i < usersCount; ++i) {

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.name = buff;
        delete[] buff;

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.surname = buff;
        delete[] buff;

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.login = buff;
        delete[] buff;

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.password = buff;
        delete[] buff;

        temp.id = User::current_user_id++;

        users[index++] = temp;
    }

    m_input.close();

    for (int i = 0; i < usersCount; ++i) {
        list.push_back(&users[i]);
    }

    return true;
}

bool FileManager::loadHistory(HistoryList &list) {

    m_input.open(historyFilePath, std::ios::binary);

    if (!m_input.is_open()) {
        std::cerr << "\n\nhistoryData.dat didn't open! FileManager::loadHistory()\n\n";
        return false;
    }

    int size{}, index{};
    char *buff{};
    H_note temp{};
    H_note *notes{};

    m_input.read((char*)&hNotesCount, sizeof(hNotesCount));
    notes = new H_note[hNotesCount]{};

    for (int i = 0; i < hNotesCount; ++i) {

        temp.item = new itm::Item{};

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.item->name = buff;
        delete[] buff;

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.item->description = buff;
        delete[] buff;

        m_input.read((char*)&temp.item->price, sizeof(temp.item->price));

        temp.user = new User{};

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.user->name = buff;
        delete[] buff;

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.user->surname = buff;
        delete[] buff;

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.user->login = buff;
        delete[] buff;

        m_input.read((char*)&size, sizeof(size));
        buff = new char[size + 1]{};
        m_input.read(buff, size);
        temp.user->password = buff;
        delete[] buff;

        m_input.read((char*)&temp.time, sizeof(temp.time));

        notes[index++] = temp;
    }

    m_input.close();

    for (int i = 0; i < hNotesCount; ++i) {
        list.push_back(&notes[i]);
    }

    return true;
}

bool FileManager::saveItems(ItemsList & list)
{
    m_output.open(itemsFilePath, std::ios::binary);

    if (!m_output.is_open()) {
        std::cerr << "\n\nItemsData.dat didn't open! saveItems()\n\n";
        return false;
    }

    int size{};

    itemsCount = list.size(); // количество товаров
    m_output.write((char*)&itemsCount, sizeof(itemsCount));

    for (auto & item : list) {

        size = item->name.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(item->name.c_str(), size);

        size = item->description.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(item->description.c_str(), size);

        m_output.write((char*)&item->price, sizeof(item->price));

    }

    m_output.close();

    return true;
}

bool FileManager::saveUsers(UsersList & list)
{
    m_output.open(usersFilePath, std::ios::binary);

    if (!m_output.is_open()) {
        std::cerr << "\n\nUsersData.dat didn't open! saveUsers()\n\n";
        return false;
    }

    int size{};

    usersCount = list.size(); // количество пользователей
    if (usersCount > 1)
        m_output.write((char*)&usersCount, sizeof(usersCount));

    for (int i = 1; i < usersCount; ++i) {

        size = list.at(i)->name.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(list.at(i)->name.c_str(), size);

        size = list.at(i)->surname.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(list.at(i)->surname.c_str(), size);

        size = list.at(i)->login.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(list.at(i)->login.c_str(), size);

        size = list.at(i)->password.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(list.at(i)->password.c_str(), size);

    }

    m_output.close();

    return true;
}

bool FileManager::saveHistory(HistoryList &list) {

    m_output.open(historyFilePath, std::ios::binary);

    if (!m_output.is_open()) {
        std::cerr << "\n\nhistoryData.dat didn't open! saveHistory()\n\n";
        return false;
    }

    int size{};

    hNotesCount = list.size();
    m_output.write((char*)&hNotesCount, sizeof(hNotesCount));

    for (int i = 0; i < hNotesCount; ++i) {

        size = list.at(i)->item->name.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(list.at(i)->item->name.c_str(), size);

        size = list.at(i)->item->description.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(list.at(i)->item->description.c_str(), size);

        m_output.write((char*)&list.at(i)->item->price, sizeof(list.at(i)->item->price));

        ///////////////////////////////

        size = list.at(i)->user->name.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(list.at(i)->user->name.c_str(), size);

        size = list.at(i)->user->surname.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(list.at(i)->user->surname.c_str(), size);

        size = list.at(i)->user->login.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(list.at(i)->user->login.c_str(), size);

        size = list.at(i)->user->password.size();
        m_output.write((char*)&size, sizeof(size));
        m_output.write(list.at(i)->user->password.c_str(), size);

        ///////////////////////////////

        m_output.write((char*)&list.at(i)->time, sizeof(list.at(i)->time));
    }

    m_output.close();

    return true;
}

FileManager* FileManager::getInstance(string usersPath, string itemsPath, string historyPath)
{
    if(instance == nullptr)
    {
        instance = new FileManager(std::move(usersPath),
                                   std::move(itemsPath),
                                   std::move(historyPath));
    }

    return instance;
}

FileManager* FileManager::getInstance()
{
    if(instance == nullptr)
    {
        instance = new FileManager("Data/usersData.dat",
                                   "Data/itemsData.dat",
                                   "Data/historyData.dat");
    }

    return instance;
}

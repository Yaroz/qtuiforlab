#ifndef PET_SHOP_FILE_MANAGER_H
#define PET_SHOP_FILE_MANAGER_H

#include <iostream>
#include <fstream>

#include <string>
#include "../Models/Item/Item.h"
#include "../Models/User/User.h"
#include "../Models/H_note/H_note.h"

using std::string;
using std::fstream;
using std::ofstream;
using std::ifstream;

class FileManager{
private:
    static string usersFilePath;
    static string itemsFilePath;
    static string historyFilePath;

    static FileManager* instance;

    int usersCount{};
    int itemsCount{};
    int hNotesCount{};

    ofstream m_output{};
    ifstream m_input{};

    FileManager(string usersPath, string itemsPath, string historyPath);

public:

    FileManager(FileManager &other) = delete;
    FileManager& operator=(const FileManager &other) = delete;

    static FileManager* getInstance(string usersPath, string itemsPath, string historyPath);
    static FileManager* getInstance();

    bool saveUsers(UsersList & list);
    bool loadUsers(UsersList & list);

    bool saveItems(ItemsList & list);
    bool loadItems(ItemsList & list);

    bool saveHistory(HistoryList & list);
    bool loadHistory(HistoryList & list);
};

#endif //PET_SHOP_FILE_MANAGER_H

#ifndef PET_SHOP_ITEM_MENU_H
#define PET_SHOP_ITEM_MENU_H

// ItemMenu - класс, с помощью которого создаются пункты меню

#include <iostream>
#include <string>
#include <functional>

using std::string;
using std::cout;

using FuncExec = std::function<int(int)>;

class ItemMenu {
public:
    ItemMenu() = delete;

    ItemMenu(string item_name, FuncExec func_exec);

    ItemMenu(const ItemMenu &);

    ItemMenu & operator=(const ItemMenu &);

    friend std::ostream & operator<<(std::ostream & out, const ItemMenu & itemMenu);

    int operator()(int selected);

    virtual string getItemName();

    virtual int exec(int selected);

private:
    string m_item_name;
    FuncExec m_func_exec;
};


#endif //PET_SHOP_ITEM_MENU_H

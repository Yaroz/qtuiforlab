#ifndef PET_SHOP_MENU_H
#define PET_SHOP_MENU_H

// Menu - класс, с помощью которого создаётся меню

#include <iostream>
#include <string>
#include <vector>
#include <ItemMenu.h>
#include <Exception.h>

using std::string;
using std::vector;
using std::cout;

using ItemList = vector<ItemMenu>;

class Menu {
public:
    Menu() = delete;

    Menu & operator=(const Menu &) = delete;

    Menu(string title, ItemList items);

    friend std::ostream & operator<<(std::ostream & out, Menu & menu);

    friend std::istream & operator>>(std::istream & in, Menu & menu);

    int operator()();

    void selectItem(int selected);

    int execSelected();

    void addItemByIndex(const ItemMenu& itemMenu, int index);

private:
    string m_title{};
    ItemList m_items{};
    int m_selected{};
};

void isOutRange(int number, int left_range, int right_range);


#endif //PET_SHOP_MENU_H

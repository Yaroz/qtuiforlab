//Item - структура, описывающая единицу товара
#ifndef PET_SHOP_ITEM_H
#define PET_SHOP_ITEM_H

#include <vector>
#include <string>

using std::string;
using std::vector;
namespace itm{
struct Item
{
    string name{"Empty name"};
    string description{"Empty description"};
    float price{};
    long int id{};

    static long int current_item_id;
};
}
using ItemsList = vector<itm::Item*>;
#endif //PET_SHOP_ITEM_H

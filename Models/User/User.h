#ifndef PET_SHOP_USER_H
#define PET_SHOP_USER_H

#include <vector>
#include <string>

using std::string;
using std::vector;

struct User 
{
    enum LevelsOfAccess
    {
        ADMIN,
        USER,
    };
	string name{"Empty name"};
	string surname{"Empty surname"};
	string login{"Empty login"};
	string password{"Empty password"};
	static long int current_user_id;
	long int id{};
	LevelsOfAccess levelAccess = USER;
};
using UsersList = vector<User*>;
#endif //PET_SHOP_USER_H

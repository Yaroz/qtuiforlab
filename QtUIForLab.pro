QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    Models\User\User.cpp \
    Models\Item\Item.cpp \
    Models/H_note/H_note.cpp\
    Store\Action.cpp\
    Store\ActionTypes.cpp\
    Store\State.cpp\
    Store\Store.cpp\
    FileManager\FileManager.cpp\
    Exception\Exception.cpp\
    Menu\ItemMenu.cpp\
    Menu\Menu.cpp


HEADERS += \
    mainwindow.h \
    Models\User\User.h\
    Models\Item\Item.h \
    Models/H_note/H_note.h\
    Store\Action.h\
    Store\ActionTypes.h\
    Store\State.h\
    Store\Store.h\
    FileManager\FileManager.h\
    Exception\Exception.h\
    Menu\ItemMenu.h\
    Menu\Menu.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

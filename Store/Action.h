#ifndef PET_SHOP_ACTION_H
#define PET_SHOP_ACTION_H

#include <Store/ActionTypes.h>

// Action - структура, связывающая тип события с данным,
//       которые это событие изменяет

struct Action {
    ActionTypes type{};
    void *data{};
};


#endif //PET_SHOP_ACTION_H

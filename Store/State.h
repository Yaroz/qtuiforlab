#ifndef PET_SHOP_STATE_H
#define PET_SHOP_STATE_H

// State - структура, описывающая данные, которые хранит приложение

#include <string>
#include <vector>
#include <map>
#include <Models/User/User.h>
#include <Models/Item/Item.h>
#include <Models/H_note/H_note.h>

using std::string;
using std::vector;
using std::map;

struct State {
    string app_name{};
    User *current_user{};
    UsersList users_list{};
    ItemsList items_list{};
    HistoryList history_list{};
    void *intent_next_screen{};
    void *intent_data{};
    bool is_push_stack{};
    void *screens_map{};
};


#endif //PET_SHOP_STATE_H

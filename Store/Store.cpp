#include "Store.h"

Store *Store::s_store{};

Store::Store(State state) : m_state(std::move(state)) {

    m_file_manager = FileManager::getInstance();

    if (bool err = _loadState() and !err) {
        std::cerr
                << "Can't load store! Store.cpp - Store()"
                << std::endl;
    }
}

Store & Store::createStore(State state) {
    static Store store(std::move(state));

    s_store = &store;

    return store;
}

Store::~Store() {

    // пытаемся сохраниться
    if (bool err = _saveState() and !err) {
        // не удалось сохранить данные
        // отображаем сообщение об этом в логе ошибок
        std::cerr
                << "Can't save store! Store.cpp - ~Store()"
                << std::endl;
    }

    // удаляем всё из state

    // удаляем пользователей
//    for (auto *pItem : m_state.users_list) {
//        delete pItem;
//    }

    // удаляем товары
//    for (auto *pItem : m_state.items_list) {
//        delete pItem;
//    }

    m_state.users_list.clear();
    m_state.items_list.clear();

}

void Store::_reducer(Action action) {

    State state(m_state);

    switch (action.type) {
        case ActionTypes::CHANGE_APP_NAME: {
            state.app_name = *static_cast<string *>(action.data);
            break;
        }
        case ActionTypes::CLEAR_CURRENT_USER: {
            state.current_user = nullptr;
            break;
        }
        case ActionTypes::SET_CURRENT_USER: {
            state.current_user = static_cast<User *>(action.data);
            break;
        }
        case ActionTypes::ADD_NEW_USER: {
            state.users_list.push_back(new User(*static_cast<User *>(action.data)));
            break;
        }
        case ActionTypes::EDIT_USER: {
            for (auto *item : state.users_list) {
                if (item->id == static_cast<User *>(action.data)->id) {
                    *item = *static_cast<User *>(action.data);
                }
            }
            break;
        }
        case ActionTypes::DELETE_USER: {
            delete state.users_list.at(*static_cast<int *>(action.data));
            state.users_list.erase(state.users_list.begin() + *static_cast<int*>(action.data));
            break;
        }
        case ActionTypes::ADD_NEW_ITEM: {
            state.items_list.push_back(new itm::Item(*static_cast<itm::Item *>(action.data)));
            break;
        }
        case ActionTypes::EDIT_ITEM_NAME: {
            for (auto item : state.items_list) {
                if (item->id == static_cast<itm::Item *>(action.data)->id) {
                    item->name = static_cast<itm::Item *>(action.data)->name;
                }
            }
            break;
        }
        case ActionTypes::EDIT_ITEM_DESCRIPTION: {
            for (auto item : state.items_list) {
                if (item->id == static_cast<itm::Item *>(action.data)->id) {
                    item->description = static_cast<itm::Item *>(action.data)->description;
                }
            }
            break;
        }
        case ActionTypes::EDIT_ITEM_PRICE: {
            for (auto item : state.items_list) {
                if (item->id == static_cast<itm::Item *>(action.data)->id) {
                    item->price = static_cast<itm::Item *>(action.data)->price;
                }
            }
            break;
        }
        case ActionTypes::DELETE_ITEM: {
            //delete state.items_list.at(*static_cast<int *>(action.data));
            state.items_list.erase(state.items_list.begin() + *static_cast<int *>(action.data));
            break;
        }
        case ActionTypes::ADD_H_NOTE: {
            state.history_list.push_back(new H_note{
               state.items_list.at(*static_cast<int *>(action.data)),
               state.current_user,
               time(nullptr)
            });
            break;
        }
        case ActionTypes::SET_INTENT_NEXT_SCREEN: {
            state.intent_next_screen = action.data;
            break;
        }
        case ActionTypes::SET_INTENT_DATA: {
            state.intent_data = action.data;
            break;
        }
        case ActionTypes::CLEAR_INTENT_NEXT_SCREEN: {
            state.intent_next_screen = nullptr;
            break;
        }
        case ActionTypes::CLEAR_INTENT_DATA: {
            state.intent_data = nullptr;
            break;
        }
        case ActionTypes::PUSH_STACK_SCREEN: {
            state.is_push_stack = true;
            break;
        }
        case ActionTypes::CLEAR_PUSH_STACK_SCREEN: {
            state.is_push_stack = false;
            break;
        }
        default:
            break;
    }

    m_state = state;
}

Store *Store::getStore() {

    try {
        if (s_store == nullptr) throw InvalidStore();
    }
    catch (InvalidStore & invalid_store) {
        std::cerr << invalid_store.what();
    }

    return s_store;
}

State Store::getState() {
    return m_state;
}

bool Store::_loadState() {

    // ТОЛЬКО ДЛЯ ТЕСТОВОГО ЗАПУСКА

    m_state.users_list.push_back(new User{
            .name = "admin",
            .surname = "admin",
            .login = "admin",
            .password = "admin",
            .id = User::current_user_id++,
            .levelAccess = User::LevelsOfAccess::ADMIN
    });
//
//    m_state.items_list = {
//            new Item{
//                "Food Royal Canin",
//                "Food for adult dogs of large breeds: 26-44 kg, 15 months-5 years",
//                1167.f,
//                Item::current_item_id++
//                },
//            new Item{
//                    "Food Purina",
//                    "Purina food for adult cats with diseases of the lower urinary tract, with oceanic fish",
//                    1419.f,
//                    Item::current_item_id++
//                },
//            new Item{
//                    "Tetra aquarium",
//                    "Aquarium for 20 liters",
//                    9256.f,
//                    Item::current_item_id++
//            }
//    };

    // ТОЛЬКО ДЛЯ ТЕСТОВОГО ЗАПУСКА

    if (!m_file_manager->loadItems(m_state.items_list)
                            ||
        !m_file_manager->loadUsers(m_state.users_list)
                            ||
        !m_file_manager->loadHistory(m_state.history_list))
        return false;

    return true;
}

bool Store::_saveState() {

    if (!m_file_manager->saveItems(m_state.items_list)
                            ||
        !m_file_manager->saveUsers(m_state.users_list)
                            ||
        !m_file_manager->saveHistory(m_state.history_list))
        return false;

    return true;
}

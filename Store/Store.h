#ifndef PET_SHOP_STORE_H
#define PET_SHOP_STORE_H


#include <iostream>
#include <Store/State.h>
#include <Store/Action.h>
#include <Exception/Exception.h>
#include <FileManager/FileManager.h>

class Store {
public:
    Store() = delete;

    Store(const Store &) = delete;

    Store & operator=(const Store &) = delete;

    static Store & createStore(State state);

    static Store * getStore();

    State getState();

    void _reducer(Action action);

    ~Store();

private:
    static Store *s_store;

    State m_state{};

    FileManager *m_file_manager;

    explicit Store(State state);

    bool _loadState();

    bool _saveState();

};


#endif //PET_SHOP_STORE_H

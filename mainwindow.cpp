#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMouseEvent>
#include <QPushButton>
#include <QHoverEvent>
#include <QWidget>
#include <QDebug>
#include <QScrollBar>
#include <QMessageBox>




QStringList MainWindow::getItemsNames()
{

    ItemsList items = m_store.getState().items_list;
    QStringList list(m_store.getState().items_list.size());
    for(int i = 0; i < (int)m_store.getState().items_list.size(); i++)
    {
        list[i] = QString::fromStdString(items[i]->name);
    }
    return list;


}
QStringList MainWindow::getItemsPrices()
{
    ItemsList items = m_store.getState().items_list;
    QStringList list(m_store.getState().items_list.size());
    for(int i = 0; i < (int)m_store.getState().items_list.size(); i++)
    {
        list[i] = QString::number(items[i]->price);
    }
    return list;

}
QStringList MainWindow::getItemsIds()
{
    ItemsList items = m_store.getState().items_list;
    QStringList list(m_store.getState().items_list.size());
    for(int i = 0; i < (int)items.size(); i++)
    {
        list[i] = QString::number(items[i]->id);
    }
    return list;
}
bool findInText(string text, string find)
{

    for(int i = 0; i < text.size(); i++)
    {
        for(int j = 0; j < find.size();j++)
        {
            if(text[i+j] != find[j])
            {
                break;
            }
            if(j == find.size()-1)
            {
                return true;
            }
        }
    }
    return false;
}
vector<int> getFindedIndexes(ItemsList items, QString find)
{
    vector<int> finded;
    for(int i = 0; i < (int)items.size(); i++)
    {
        std::string text = items[i]->name;
        if(findInText(QString::fromStdString(text).toLower().toStdString(),find.toLower().toStdString()) ||
                find == QString::number(items[i]->price) ||
                find == QString::number(items[i]->id))
        {
            finded.push_back(i);
        }

    }
    return finded;
}
QStringList MainWindow::getHistoryNames()
{
    HistoryList hlist = m_store.getState().history_list;
    QStringList list(hlist.size());

    for(int i = 0; i < (int)hlist.size();i++)
    {
        list[i] = QString::fromStdString(hlist[i]->item->name);
    }
    return list;

}
QStringList MainWindow::getHistoryPrices()
{
    HistoryList hlist = m_store.getState().history_list;
    QStringList list(hlist.size());

    for(int i = 0; i < (int)hlist.size();i++)
    {
        list[i] = QString::number(hlist[i]->item->price);
    }
    return list;

}
QStringList MainWindow::getHistoryDates()
{
    HistoryList hlist = m_store.getState().history_list;
    QStringList list(hlist.size());

    for(int i = 0; i < (int)hlist.size();i++)
    {
        tm* time = localtime(&hlist[i]->time);
        char buffer[64];
        strftime(buffer,64,"%d.%m.%Y",time);
        string realTime = string(buffer);
        list[i] = QString::fromStdString(realTime);
    }
    return list;
}
QStringList MainWindow::getHistoryUsers()
{
    HistoryList hlist = m_store.getState().history_list;
    QStringList list(hlist.size());

    for(int i = 0; i < (int)hlist.size();i++)
    {
        list[i] = QString::fromStdString(hlist[i]->user->login);
    }
    return list;

}
void MainWindow::updateHistory()
{
    HistoryList hlist = m_store.getState().history_list;

    ui->historyTable->clearSelection();
    ui->historyTable->selectionModel()->clearSelection();
    ui->historyTable->setRowCount(1);
    ui->historyTable->setColumnCount(4);
    ui->historyTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->historyTable->setColumnWidth(0,300);
    ui->historyTable->setColumnWidth(1,80);
    ui->historyTable->setColumnWidth(2,120);
    ui->historyTable->setColumnWidth(3,200);

    QStringList hnames = getHistoryNames();
    QStringList hprices = getHistoryPrices();
    QStringList hdates = getHistoryDates();
    QStringList husers = getHistoryUsers();
    int k = 0;
    for(int i = 0; i < (int)hlist.size();i++)
    {
        if(husers[i].toStdString() ==m_store.getState().current_user->login || m_store.getState().current_user->name == "admin")
        {
            QTableWidgetItem* tempName = new QTableWidgetItem(hnames[i]);
            QTableWidgetItem* tempPrice = new QTableWidgetItem(hprices[i]);
            QTableWidgetItem* tempDate = new QTableWidgetItem(hdates[i]);
            QTableWidgetItem* tempUser = new QTableWidgetItem(husers[i]);
            tempPrice->setTextAlignment(Qt::AlignCenter);
            tempDate->setTextAlignment(Qt::AlignCenter);
            tempUser->setTextAlignment(Qt::AlignCenter);
            ui->historyTable->setItem(k,0,tempName);
            ui->historyTable->setItem(k,1,tempPrice);
            ui->historyTable->setItem(k,2,tempDate);
            ui->historyTable->setItem(k,3,tempUser);
            k++;
            ui->historyTable->setRowCount(k+1);
        }
    }
}
void MainWindow::updateItems()
{
    currentItem = -1;
    ui->itemsTable->clearSelection();
    ui->itemsTable->selectionModel()->clearSelection();
    //ui->itemsTable->clear();
    ui->itemsTable->setRowCount(1);


    ui->itemsTable->setColumnCount(3);
    ui->itemsTable->setRowCount(m_store.getState().items_list.size() - blockedItems.size());
    ui->itemsTable->setColumnWidth(0,25);
    ui->itemsTable->setColumnWidth(1,250);
    ui->itemsTable->setColumnWidth(2,80);
    ui->itemsTable->setSelectionBehavior(QAbstractItemView::SelectRows);

    QStringList itemsNames = getItemsNames();
    QStringList itemsIds = getItemsIds();
    QStringList itemsPrices = getItemsPrices();
    int k = 0;

    for(int i = 0; i < (int)m_store.getState().items_list.size(); i++)
    {
        bool isBlocked = false;
        for(int j : blockedItems)
        {
            if(itemsIds[i].toInt() == (j+1))
            {
                isBlocked = true;
            }
        }
        if(!isBlocked || blockedItems.size() == m_store.getState().items_list.size())
        {
            QTableWidgetItem * tempName = new QTableWidgetItem(itemsNames[i]);
            QTableWidgetItem * tempId = new QTableWidgetItem(itemsIds[i]);
            tempId->setTextAlignment(Qt::AlignCenter);
            QTableWidgetItem * tempPrice = new QTableWidgetItem(itemsPrices[i]);
            tempPrice->setTextAlignment(Qt::AlignCenter);
            ui->itemsTable->setItem(k,1,tempName);
            ui->itemsTable->setItem(k,0,tempId);
            ui->itemsTable->setItem(k,2,tempPrice);
            k++;

        }

    }
}
void MainWindow::updateItems(QStringList itemsIds, QStringList itemsNames, QStringList itemsPrices)
{
    currentItem = -1;
    ui->itemsTable->clearSelection();
    ui->itemsTable->selectionModel()->clearSelection();
    //ui->itemsTable->clear();
    ui->itemsTable->setRowCount(1);


    ui->itemsTable->setColumnCount(3);
    ui->itemsTable->setRowCount(m_store.getState().items_list.size() - blockedItems.size());
    ui->itemsTable->setColumnWidth(0,25);
    ui->itemsTable->setColumnWidth(1,250);
    ui->itemsTable->setColumnWidth(2,80);
    ui->itemsTable->setSelectionBehavior(QAbstractItemView::SelectRows);

    //QStringList itemsNames = getItemsNames();
    //QStringList itemsIds = getItemsIds();
    //QStringList itemsPrices = getItemsPrices();
    int k = 0;

    for(int i = 0; i < (int)m_store.getState().items_list.size(); i++)
    {
        bool isBlocked = false;
        for(int j : blockedItems)
        {
            if(itemsIds[i].toInt() == (j+1))
            {
                isBlocked = true;
            }
        }
        if(!isBlocked || blockedItems.size() == m_store.getState().items_list.size())
        {
            QTableWidgetItem * tempName = new QTableWidgetItem(itemsNames[i]);
            QTableWidgetItem * tempId = new QTableWidgetItem(itemsIds[i]);
            tempId->setTextAlignment(Qt::AlignCenter);
            QTableWidgetItem * tempPrice = new QTableWidgetItem(itemsPrices[i]);
            tempPrice->setTextAlignment(Qt::AlignCenter);
            ui->itemsTable->setItem(k,1,tempName);
            ui->itemsTable->setItem(k,0,tempId);
            ui->itemsTable->setItem(k,2,tempPrice);
            k++;

        }

    }
}



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_store(Store::createStore(State{.app_name = "Pet shop"}))
{
    MainWindow::setWindowIcon(QIcon("images/checklist.png"));
    Qt::WindowFlags flags = windowFlags();
    setWindowFlags(flags | Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground, true);
    show();
    ui->setupUi(this);
    MainWindow::move(100,50);

    updateItems();

    QPixmap pixmap("images/saveIconMono.png");
    QIcon buttonIcon(pixmap);

    ui->descSaveButton->setIcon(buttonIcon);
    ui->descSaveButton->setIconSize(QSize(26,26));
    ui->registerFrame->setEnabled(false);
    ui->registerFrame->hide();
    ui->logInToButton->setEnabled(false);
    ui->logInToButton->hide();

    ui->authFrame->setEnabled(true);
    ui->authFrame->show();

    ui->BackGround->setEnabled(false);
    ui->BackGround->hide();

    ui->historyFrame->setEnabled(false);
    ui->historyFrame->hide();



}
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if(canMoveWindow)
    {
        MainWindow::move(event->pos() - mousePos + MainWindow::pos());
    }

}
void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton && event->pos().y() < 26)
    {
        canMoveWindow = true;
        mousePos = event->pos();
    }

}
void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        canMoveWindow = false;
    }
}

void MainWindow::on_exitButton_clicked()
{
    MainWindow::close();
}



void MainWindow::on_exitButton_pressed()
{

}



void MainWindow::on_exitButton_released()
{

}


void MainWindow::on_hideButton_clicked()
{
    MainWindow::showMinimized();
}


void MainWindow::on_addItemButton_clicked()
{
    m_store._reducer(Action{

                     ActionTypes::ADD_NEW_ITEM,
                         static_cast<void*>(new itm::Item{
                             "Название товара",
                             "Описание отвара",
                             0,
                             itm::Item::current_item_id++
                         })});
    updateItems();
}
void MainWindow::on_deleteItemButton_clicked()
{
    //Сломано!!!
    if(ui->itemsTable->rowCount() > 0 && ui->itemsTable->currentRow() >= 0)
    {
        int toDel = ui->itemsTable->item(ui->itemsTable->currentRow(),0)->text().toInt();
        int delIndex = -1;

        for(int i = 0; i < (int)m_store.getState().items_list.size();i++)
        {
            if(m_store.getState().items_list.at(i)->id == toDel)
            {
                delIndex = i;
            }
        }
        if(delIndex != -1)
        {
            m_store._reducer(Action{
                                 ActionTypes::DELETE_ITEM,
                                 static_cast<void *>(&delIndex)
                             });

        }
        updateItems();
    }
}


void MainWindow::on_itemsTable_currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous)
{

    if(startEdit || descriptionChanged)
    {
        QPixmap pixmap("images/saveIconMono.png");
        QIcon buttonIcon(pixmap);
        ui->descSaveButton->setIcon(buttonIcon);
        ui->descSaveButton->setIconSize(QSize(26,26));
        descriptionChanged = false;
        startEdit = false;
    }

    if(ui->itemsTable->rowCount() > 0 && ui->itemsTable->currentRow() >= 0)
    {
        int findId = ui->itemsTable->item(current->row(),0)->text().toInt();
        string findedText{};
        for(auto i : m_store.getState().items_list)
        {
            if(i->id == findId)
            {
                findedText = i->description;
            }
        }
        ui->itemDescription->setText(QString::fromStdString(findedText));
    }
   currentItem = ui->itemsTable->currentRow();
}


void MainWindow::on_btn_2_clicked()
{
    if(itemNameClick%2 == 0)
    {
        ui->itemsTable->sortByColumn(1,Qt::AscendingOrder);
    }
    else
    {
        ui->itemsTable->sortByColumn(1,Qt::DescendingOrder);
    }
    itemNameClick++;
}


void MainWindow::on_btn_3_clicked()
{
    ItemsList tempItems = m_store.getState().items_list;
    for(int i = 0; i < (int)tempItems.size() - 1; i++)
        {
            for(int j = 0; j < (int)tempItems.size() - i - 1; j++)
            {
                if((tempItems[j]->price > tempItems[j + 1]->price) && itemPriceClick%2 == 0)
                {
                    std::swap(tempItems[j],
                              tempItems[j+1]);
                }
                else if((tempItems[j]->price < tempItems[j + 1]->price)&& itemPriceClick%2 == 1)
                {
                    std::swap(tempItems[j],
                              tempItems[j+1]);
                }

            }
        }
    QStringList pricesList(tempItems.size());
    QStringList namesList(tempItems.size());
    QStringList idsList(tempItems.size());
    for(int i = 0; i < (int)tempItems.size(); i++)
    {
        pricesList[i] = QString::number(tempItems[i]->price);
        namesList[i] = QString::fromStdString(tempItems[i]->name);
        idsList[i] = QString::number(tempItems[i]->id);
    }
    updateItems(idsList,namesList,pricesList);
    itemPriceClick++;

}
void MainWindow::on_btn_clicked()
{
    ItemsList tempItems = m_store.getState().items_list;

    for(int i = 0; i < (int)tempItems.size() - 1; i++)
        {
            for(int j = 0; j < (int)tempItems.size() - i - 1; j++)
            {
                if((tempItems[j]->id > tempItems[j + 1]->id) && itemIdClick%2 == 0)
                {
                    std::swap(tempItems[j],
                              tempItems[j+1]);
                }
                else if((tempItems[j]->id < tempItems[j + 1]->id)&& itemIdClick%2 == 1)
                {
                    std::swap(tempItems[j],
                              tempItems[j+1]);
                }

            }
        }
    QStringList pricesList(tempItems.size());
    QStringList namesList(tempItems.size());
    QStringList idsList(tempItems.size());
    for(int i = 0; i < (int)tempItems.size(); i++)
    {
        pricesList[i] = QString::number(tempItems[i]->price);
        namesList[i] = QString::fromStdString(tempItems[i]->name);
        idsList[i] = QString::number(tempItems[i]->id);
    }
    updateItems(idsList,namesList,pricesList);
    itemIdClick++;

}


void MainWindow::on_findLine_textEdited(const QString &arg1)
{
    if(ui->findLine->text().size() > 0)
    {
        vector<int> finded = getFindedIndexes(m_store.getState().items_list,ui->findLine->text());
        ItemsList items = m_store.getState().items_list;
        blockedItems.clear();
        for(int i = 0; i < (int)items.size();i++)
        {
            bool toBlock = true;
            for(int k = 0; k < (int)finded.size(); k++)
            {
                if(i == finded[k])
                {
                    toBlock = false;
                }
            }
            if(toBlock)
            {
                blockedItems.push_back(i);
            }
        }
    }
    else
    {
        blockedItems.clear();
    }
    updateItems();

}



void MainWindow::on_itemsTable_itemDoubleClicked(QTableWidgetItem *item)
{
    startEdit = true;
}


void MainWindow::on_itemsTable_itemChanged(QTableWidgetItem *item)
{
    if(startEdit)
    {
        itm::Item *newItem = new itm::Item{};
        int newId = ui->itemsTable->item(item->row(),0)->text().toInt();
        newItem->id = newId;
        if(item->column() == 2)
        {
            int newPrice = item->text().toInt();
            newItem->price = newPrice;
            m_store._reducer(Action{
                                 ActionTypes::EDIT_ITEM_PRICE,
                                 static_cast<void *>(newItem)

                             });
        }
        else if(item->column() == 1)
        {
            string newName = item->text().toStdString();
            newItem->name = newName;
            m_store._reducer(Action{
                                 ActionTypes::EDIT_ITEM_NAME,
                                 static_cast<void*>(newItem)
                             });

        }
        delete newItem;
        startEdit = false;

    }
}




void MainWindow::on_itemDescription_textChanged()
{
    if(!descriptionChanged && ui->itemsTable->currentRow() >= 0 && currentItem == ui->itemsTable->currentRow() )
    {
        QPixmap pixmap("images/saveIconColored.png");
        QIcon buttonIcon(pixmap);

        ui->descSaveButton->setIcon(buttonIcon);
        ui->descSaveButton->setIconSize(QSize(26,26));

        descriptionChanged = true;
    }
}


void MainWindow::on_descSaveButton_clicked()
{
    if(descriptionChanged)
    {
        QPixmap pixmap("images/saveIconMono.png");
        QIcon buttonIcon(pixmap);
        ui->descSaveButton->setIcon(buttonIcon);
        ui->descSaveButton->setIconSize(QSize(26,26));


        itm::Item *newItem = new itm::Item{};
        int newId = ui->itemsTable->item(ui->itemsTable->currentRow(),0)->text().toInt();
        newItem->id = newId;

        string newDesc = ui->itemDescription->toPlainText().toStdString();
        newItem->description = newDesc;

        m_store._reducer(Action{
                             ActionTypes::EDIT_ITEM_DESCRIPTION,
                             static_cast<void*>(newItem)
                         });



        delete newItem;
        descriptionChanged = false;
    }
}


void MainWindow::disableAdminFunctions()
{
    ui->addItemButton->setEnabled(false);
    ui->addItemButton->hide();

    ui->deleteItemButton->setEnabled(false);
    ui->deleteItemButton->hide();

    ui->descSaveButton->setEnabled(false);
    ui->descSaveButton->hide();

    ui->itemsTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->itemDescription->setReadOnly(true);
}
void MainWindow::enableAdminFunctions()
{
    ui->addItemButton->setEnabled(true);
    ui->addItemButton->show();

    ui->deleteItemButton->setEnabled(true);
    ui->deleteItemButton->show();

    ui->descSaveButton->setEnabled(true);
    ui->descSaveButton->show();

    ui->itemsTable->setEditTriggers(QAbstractItemView::DoubleClicked);
    ui->itemDescription->setReadOnly(false);
}

void MainWindow::on_LogInButton_clicked()
{
    string login = ui->loginEdit->text().toStdString();
    string password = ui->passEdit->text().toStdString();
    UsersList users = m_store.getState().users_list;
    User* currentUser;

    bool acces = false;

    for(User* u : users)
    {
        if(u->login == login && u->password == password)
        {
            currentUser = u;
            acces = true;
            break;
        }
    }
    if(acces)
    {
        m_store._reducer(Action{
                             ActionTypes::SET_CURRENT_USER,
                             static_cast<void*>(currentUser)

                         });
        if(currentUser->name != "admin")
        {
             disableAdminFunctions();
        }
        else
        {
            enableAdminFunctions();
        }
        ui->authFrame->setEnabled(false);
        ui->authFrame->hide();
        ui->BackGround->setEnabled(true);
        ui->BackGround->show();
    }
    else
    {
        QMessageBox::warning(this,"Неверные данные","Логин или пароль введены неверно, либо данный аккаунт не существует.");

    }

}




void MainWindow::on_registerStartButton_clicked()
{
    ui->loginFrame->setEnabled(false);
    ui->loginFrame->hide();
    ui->registerFrame->setEnabled(true);
    ui->registerFrame->show();
    ui->logInToButton->show();
    ui->registerStartButton->hide();
    ui->logInToButton->setEnabled(true);
    ui->registerStartButton->setEnabled(false);
}


void MainWindow::on_logInToButton_clicked()
{
    ui->registerFrame->setEnabled(false);
    ui->registerFrame->hide();
    ui->loginFrame->setEnabled(true);
    ui->loginFrame->show();
    ui->logInToButton->hide();
    ui->logInToButton->setEnabled(false);
    ui->registerStartButton->show();
    ui->registerStartButton->setEnabled(true);

}


void MainWindow::on_registerButton_clicked()
{
    string name = ui->nameEdit->text().toStdString();
    string surname = ui->surnameEdit->text().toStdString();
    string login = ui->loginRegEdit->text().toStdString();
    string password = ui->passRegEdit->text().toStdString();

    UsersList users = m_store.getState().users_list;

    bool isExist = false;

    for(auto u : users)
    {
        if(u->login == login)
        {
            isExist = true;
            break;
        }
    }

    if(!isExist)
    {
        User* newUser = new User{};
        newUser->id = User::current_user_id++;
        newUser->login = login;
        newUser->password = password;
        newUser->name = name;
        newUser->surname = surname;

        m_store._reducer(Action{
                             ActionTypes::ADD_NEW_USER,
                             static_cast<void*>(newUser)
                         });
        m_store._reducer(Action{
                             ActionTypes::SET_CURRENT_USER,
                             static_cast<void*>(newUser)
                         });

        if(newUser->name == "admin")
        {
            enableAdminFunctions();
        }
        else
        {
            disableAdminFunctions();
        }
        ui->authFrame->setEnabled(false);
        ui->authFrame->hide();
        ui->BackGround->setEnabled(true);
        ui->BackGround->show();

    }
    else
    {
        QMessageBox::warning(this,"Аккаунт уже существует","Аккаунт с данным логином уже существует.");
    }
}


void MainWindow::on_ExitAppButton_clicked()
{
    MainWindow::close();
}


void MainWindow::on_accExitButton_clicked()
{
    m_store._reducer(Action{
                         ActionTypes::CLEAR_CURRENT_USER
                     });
    ui->BackGround->setEnabled(false);
    ui->BackGround->hide();
    ui->authFrame->setEnabled(true);
    ui->authFrame->show();
    ui->loginFrame->setEnabled(true);
    ui->loginFrame->show();
    ui->registerStartButton->setEnabled(true);
    ui->registerStartButton->show();
    ui->logInToButton->setEnabled(false);
    ui->logInToButton->hide();
    ui->registerFrame->setEnabled(false);
    ui->registerFrame->hide();
    ui->logInToButton->setEnabled(false);
    ui->logInToButton->hide();

    ui->loginEdit->clear();
    ui->passEdit->clear();

    ui->nameEdit->clear();
    ui->surnameEdit->clear();
    ui->loginRegEdit->clear();
    ui->passRegEdit->clear();



}


void MainWindow::on_buyItem_clicked()
{
    if(ui->itemsTable->currentRow() >= 0)
    {
        int itemId = ui->itemsTable->item(ui->itemsTable->currentRow(),0)->text().toInt();
        int itemIndex;
        ItemsList items = m_store.getState().items_list;
        bool isExist = false;

        for(int i = 0; i < (int)items.size(); i++)
        {
            if(items.at(i)->id == itemId)
            {
                itemIndex = i;
                isExist = true;
                break;
            }
        }

        if(isExist)
        {
            m_store._reducer(Action{
                                 ActionTypes::ADD_H_NOTE,
                                 static_cast<void*>(&itemIndex)
                             });
        }
        QMessageBox::about(this,"Покупка товара","Товар успешно куплен. Можете посмотреть информация в списке заказов." );

    }
}

void MainWindow::on_showHistoryButton_clicked()
{
    updateHistory();

    ui->historyFrame->setEnabled(true);
    ui->historyFrame->show();
}


void MainWindow::on_backToMenu_clicked()
{
    ui->historyFrame->setEnabled(false);
    ui->historyFrame->hide();
    updateItems();

}


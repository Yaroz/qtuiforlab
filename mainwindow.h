#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Store/Store.h"
#include "Store/State.h"
#include "FileManager/FileManager.h"
#include "Models/Item/Item.h"
#include <QTableWidget>
#include <QTableWidgetItem>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void hideAll();

    QStringList getItemsNames();
    QStringList getItemsPrices();
    QStringList getItemsIds();

    QStringList getHistoryNames();
    QStringList getHistoryPrices();
    QStringList getHistoryDates();
    QStringList getHistoryUsers();

    void updateItems();
    void updateItems(QStringList itemsIds, QStringList itemsNames, QStringList itemsPrices);
    void updateHistory();
    void disableAdminFunctions();
    void enableAdminFunctions();

private slots:
    void on_exitButton_clicked();

    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);


    void on_exitButton_pressed();
    void on_exitButton_released();



    void on_hideButton_clicked();

    void on_addItemButton_clicked();

    void on_itemsTable_currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous);

    void on_btn_2_clicked();

    void on_btn_3_clicked();

    void on_btn_clicked();


    void on_findLine_textEdited(const QString &arg1);

    void on_itemsTable_itemDoubleClicked(QTableWidgetItem *item);

    void on_itemsTable_itemChanged(QTableWidgetItem *item);


    void on_itemDescription_textChanged();

    void on_descSaveButton_clicked();

    void on_deleteItemButton_clicked();

    void on_LogInButton_clicked();

    void on_registerStartButton_clicked();

    void on_logInToButton_clicked();

    void on_registerButton_clicked();

    void on_ExitAppButton_clicked();

    void on_accExitButton_clicked();

    void on_buyItem_clicked();

    void on_showHistoryButton_clicked();

    void on_backToMenu_clicked();

private:
    Ui::MainWindow *ui;
    bool canMoveWindow = false;
    QPoint mousePos;
    Store &m_store;
    int currentItem = -1;

    int itemNameClick = 0;
    int itemIdClick = 0;
    int itemPriceClick = 0;

    bool startEdit = false;
    bool descriptionChanged = false;

    vector<int> blockedItems;

};
#endif // MAINWINDOW_H
